# Dev Log - Planned Deletion Emails
## CANIN Corentin, MONTEILLER Joshua, WAGNER Samy
 
### 17/01/2022
 
Project subject reading, we are starting to discover the WebExtensions API used to create Thunderbird extensions.
No commits at the moment, we are working together using Visual Studio Code’s LiveShare. We started by trying to create a button usable when writing an email to allow the user to input and change the deletion date among other things.
 
### 24/01/2022
 
Realization of a basic popup for the interface with the user (Absolute/relative date, text field).
 
![pop-up](https://cdn.discordapp.com/attachments/932341085949276273/935202719164145734/unknown.png)
 
It seems that it’s impossible to add our own fields in the composing menu of a mail. We need to use a text field in the popup.
We search for documentation to understand how to add a deleting date field in a mail. We have decided that we will add a field in the head of the HTML who composed the mail.
Possibility of building the deleting condition like a XML tree to make the writing and reading easier.
 
Example :
 
![xml](https://cdn.discordapp.com/attachments/932341085949276273/935208713000722502/unknown.png)
 
To Do : A list of issues Thunderbird is posing regarding the features we are asked to implement - present it to Mr. PERIN and talk with him about it.
 
### 31/01/2022
 
### List of features to discuss with Mr. PERIN :
The WebExtensions API for Thunderbird isn’t very developed yet and doesn’t allow us to implement everything we would like to :
 
* Adding a new text field under the subject field to input manually le deletion condition (Thunderbird allows the user to add it manually but we can’t force its creation from the extension).
 
* We are unable to know if the mail sent is plain text or HTML which is problematic to store the deletion condition (storing it in the mail body isn’t very pretty…).
 
* Is an “expired” folder really necessary when we have the trash folder ? The mails would never be truly deleted.
 
### Following our discussion with Mr. PERIN :
 
We should prioritize basic features and shouldn’t lose our focus (top priority : absolute date deletion).
 
We need to have something we can present for the oral presentation. Many elements proposed are mostly ideas thrown in the subject rather than priorities.
 
We also need to find a way to not check messages to delete too often because it is a way too heavy design.
 
### 07/02/2022
 
We have decided that we will stock the mail which will be deleted in a json file with their id and theirs conditions and thanks to the Storage API. We decided to limit the condition format to “ABSOLUTE DATE or WHEN READING + X TIME”.
 
This is a example of our format that we will use : at the top of the board, the json structure who will stocks the mails to delete, at the bottom, the XML syntax who will served to stock the condition in the head of the HTML in the body of the mail :
 
<img src="https://cdn.discordapp.com/attachments/932642726594379827/940276442854473829/20220207_170157.jpg" alt="syntaxe" width="500"/>


Finally, we started working on how to get the condition in the received mails (using the onNewMailReceived event) we will be using to later build the json file.
 
### 14/02/2022
 
Rewriting the dev log in english :)
We also tried to store the conditions in the HTML of the mails but we were not successful.

### 28/02/2022

Today was mid-term presentation so we dedicated the day to preparing our oral presentation.
After the presentation we discussed our choices with Mr. PALIX, Mr. RICHARD and Mr. DONSEZ. We were advised to look for Thunderbird add-ons with similar purposes to understand how they manage emails. It was also proposed to use attached files to store the conditions. That way, plain-text emails will be supported while staying (almost) invisible for the user. We need to run some tests to determine which methode is the lightest/most useful.

### 29/02/2022

We decided to work on other things today instead of the project.

### 07/03/2022

We finally decided to shift the storage of the condition to attached files. The file is called __dctimedata__ and is formatted as such :

* Absolute date to delete the mail at in [ISO 8601 format](https://en.wikipedia.org/wiki/ISO_8601) which is fully supported by JavaScript Date objects. It is a compact and easily parsable format for us to use.
* A space
* The delay after reading the mail when we will delete the mail in this format : NumberOfYears|NumberOfMonths|NumberOfDays|NumberOfHours|NumberOfMinutes

The space character will always be there, even if one of the conditions (absolute date or on reading delay) is missing. Thus, if the absolute date is missing, the file will start with a space. If a number in between the pipes is missing, it will be interpreted as 0. If the pipes are missing, there are no on reading condition.

We revised our local storage design. It is divided in two parts : abs and onRead.
The abs part will store ids, ordered by date of deletion planned. The onRead part consists of couples (delay, absolute date) stored under a key corresponding to the id of the mail. On the event of reading the corresponding mail, the entry will be deleted from the onRead part and one will be created in the abs part at a date corresponding the time of reading + the delay. The absolute date stored with the delay is there to easily know which entry to edit in the abs part if there was both the absolute and relative conditions enabled.

<img src="https://cdn.discordapp.com/attachments/932642726594379827/950396021723267113/20220307_151315.jpg" alt="new storage" width="500"/>

We started to write code to store our data in the local storage from the format read from the attached file.

### 08/03/2022

More programming.

### 14/03/2022

Even more programming. Pass.

### 15/03/2022

Fixed the way we checked if the deletion dates expired. Now, we check if the year expired, if so we delete all, else if the year is the same as today's year, we check if the month expired and so on...
Started working on the menu pop-up where the user will setup the folder he wants the deleted emails to be moved to as well as the frequency the add-on should check if emails are to be deleted.
Worked on fixing the pre-condition cleanup in order to have conditions that are ready to be sent using attached files.

### 21/03/2022

Today we worked more on the menu pop-up. It consists of a list of folders from which the user will choose the folder where the expired mail will be sent and some selectors to choose at what frequency the add-on should check if any mail expired.
The chosen folder's path is stored in the local storage. We plan to do so for the frequency too. Then we will only have to use this data to actually check if any mail expired and our add-on's base features will be complete.

<img src="https://cdn.discordapp.com/attachments/932341085949276273/955497450074951680/unknown.png" alt="menu1" width="500"/>
<img src="https://cdn.discordapp.com/attachments/932341085949276273/955497543792464012/unknown.png" alt="menu2" width="500"/>

### 28/03/2022

The last week-end, we finished to work on the code and today, we have prepared the report. And we now have a final logo for the addon ! 

<img src="https://cdn.discordapp.com/attachments/932341085949276273/957451393575309312/pde.png" alt="menu2" width="100"/>
